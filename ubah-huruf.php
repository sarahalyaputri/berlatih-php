<?php
function ubah_huruf($string)
{
  $abjab = "abcdefghijklmnopqrstuvwxyz";
  $output = "";
  for ($i = 0; $i < strlen($string); $i++){
      $position = strpos($abjab, $string[$i]);
      $output .= substr($abjab, $position + 1, 1);
  }
  return $output ;
//kode di sini

}

// TEST CASES
echo ubah_huruf('wow');
echo "<br>";// xpx
echo ubah_huruf('developer');
echo "<br>";// efwfmpqfs
echo ubah_huruf('laravel');
echo "<br>"; // mbsbwfm
echo ubah_huruf('keren');
echo "<br>";// lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>
