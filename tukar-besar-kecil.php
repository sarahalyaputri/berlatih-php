<?php
function tukar_besar_kecil($string)
{
    $abjadKecil = "abcdefghijklmnoprstuvwxyz";
    $output = "";
    for ($i = 0; $i < strlen($string); $i++){
      $apakah_kecil = strpos($abjadKecil, $string[$i]);
      if ($apakah_kecil == null) {
        $output .= strtolower($string[$i]);
      } else {
        $output .= strtoupper($string[$i]);
      }
    }
    return $output . "<br>";
//kode di sini
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>
